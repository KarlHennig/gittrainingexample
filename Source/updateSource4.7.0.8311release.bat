@ECHO OFF
IF NOT EXIST Examples GOTO ExamplesEnd
cd Examples
hg up -r 993f27927224
cd ..
:ExamplesEnd
IF NOT EXIST RainfallRunoff GOTO RainfallRunoffEnd
cd RainfallRunoff
hg up -r 355008efbe71
cd ..
:RainfallRunoffEnd
IF NOT EXIST StochasticClimate GOTO StochasticClimateEnd
cd StochasticClimate
hg up -r f0a87ac7d547
cd ..
:StochasticClimateEnd
IF NOT EXIST RiverSystem GOTO RiverSystemEnd
cd RiverSystem
hg up -r 6072e9838722
cd ..
:RiverSystemEnd
IF NOT EXIST Ecology GOTO EcologyEnd
cd Ecology
hg up -r 7a85c21d3714
cd ..
:EcologyEnd
IF NOT EXIST TIME GOTO TIMEEnd
cd TIME
hg up -r 3dca6b0ddc74
cd ..
:TIMEEnd
IF NOT EXIST SourcePlugin.Dynamic_SedNet GOTO SourcePlugin.Dynamic_SedNetEnd
cd SourcePlugin.Dynamic_SedNet
hg up -r cbe9cd036dc9
cd ..
:SourcePlugin.Dynamic_SedNetEnd
IF NOT EXIST SourcePlugin.RRModelInputExporter GOTO SourcePlugin.RRModelInputExporterEnd
cd SourcePlugin.RRModelInputExporter
hg up -r 22e9c5f48304
cd ..
:SourcePlugin.RRModelInputExporterEnd
IF NOT EXIST SourcePlugin.DERMTools GOTO SourcePlugin.DERMToolsEnd
cd SourcePlugin.DERMTools
hg up -r ae1b0bc39d4f
cd ..
:SourcePlugin.DERMToolsEnd
IF NOT EXIST SourcePlugin.SKM.Geofabric GOTO SourcePlugin.SKM.GeofabricEnd
cd SourcePlugin.SKM.Geofabric
hg up -r cf7d1d329163
cd ..
:SourcePlugin.SKM.GeofabricEnd
IF NOT EXIST SourcePlugin.FarmDams GOTO SourcePlugin.FarmDamsEnd
cd SourcePlugin.FarmDams
hg up -r 5696442482a0
cd ..
:SourcePlugin.FarmDamsEnd
IF NOT EXIST Insight GOTO InsightEnd
cd Insight
hg up -r f9bba969de6e
cd ..
:InsightEnd
IF NOT EXIST 3rdParty GOTO 3rdPartyEnd
cd 3rdParty
hg up -r a7aa0ac31f23
cd ..
:3rdPartyEnd
IF NOT EXIST SourcePlugin.CSIRO.RiverMurrayDSS GOTO SourcePlugin.CSIRO.RiverMurrayDSSEnd
cd SourcePlugin.CSIRO.RiverMurrayDSS
hg up -r e6911f40703c
cd ..
:SourcePlugin.CSIRO.RiverMurrayDSSEnd
IF NOT EXIST SourcePlugin.FlowAdjustment GOTO SourcePlugin.FlowAdjustmentEnd
cd SourcePlugin.FlowAdjustment
hg up -r 599641eaadda
cd ..
:SourcePlugin.FlowAdjustmentEnd
IF NOT EXIST SourcePlugin.DataSourcesTools GOTO SourcePlugin.DataSourcesToolsEnd
cd SourcePlugin.DataSourcesTools
hg up -r df1506772fb1
cd ..
:SourcePlugin.DataSourcesToolsEnd
IF NOT EXIST SourcePlugin.CSIRO.dSedNet GOTO SourcePlugin.CSIRO.dSedNetEnd
cd SourcePlugin.CSIRO.dSedNet
hg up -r e5492afd51a5
cd ..
:SourcePlugin.CSIRO.dSedNetEnd
IF NOT EXIST Reef_Modelling GOTO Reef_ModellingEnd
cd Reef_Modelling
hg up -r f6a181770d1f
cd ..
:Reef_ModellingEnd
IF NOT EXIST SourcePlugin.MDBAPerformance GOTO SourcePlugin.MDBAPerformanceEnd
cd SourcePlugin.MDBAPerformance
hg up -r 913257ba7074
cd ..
:SourcePlugin.MDBAPerformanceEnd
IF NOT EXIST SourcePlugin.BorderRiversPlugin GOTO SourcePlugin.BorderRiversPluginEnd
cd SourcePlugin.BorderRiversPlugin
hg up -r f2b86e9a5fd6
cd ..
:SourcePlugin.BorderRiversPluginEnd
IF NOT EXIST SourcePlugin.GR4JSG GOTO SourcePlugin.GR4JSGEnd
cd SourcePlugin.GR4JSG
hg up -r f4aeba2675bc
cd ..
:SourcePlugin.GR4JSGEnd
IF NOT EXIST Veneer GOTO VeneerEnd
cd Veneer
git checkout d781fce602db61d5b4c256a0ddafa63fe186fb73
cd ..
:VeneerEnd
IF NOT EXIST UrbanDeveloper GOTO UrbanDeveloperEnd
cd UrbanDeveloper
hg up -r 30ca71221f2d
cd ..
:UrbanDeveloperEnd
IF NOT EXIST Source.Metadata GOTO Source.MetadataEnd
cd Source.Metadata
hg up -r d91d5316d412
cd ..
:Source.MetadataEnd
IF NOT EXIST SourcePlugin.MDBAPerformance2 GOTO SourcePlugin.MDBAPerformance2End
cd SourcePlugin.MDBAPerformance2
hg up -r 520af67b74f6
cd ..
:SourcePlugin.MDBAPerformance2End
IF NOT EXIST Dodoc-Source GOTO Dodoc-SourceEnd
cd Dodoc-Source
git checkout 5fc35f887d0dceb140901dcdbaf5fcb26186222a
cd ..
:Dodoc-SourceEnd
IF NOT EXIST MDBAPlugins GOTO MDBAPluginsEnd
cd MDBAPlugins
hg up -r 1018b68f8004
cd ..
:MDBAPluginsEnd
IF NOT EXIST MDBA.ModelManager GOTO MDBA.ModelManagerEnd
cd MDBA.ModelManager
hg up -r 48f63bb5964f
cd ..
:MDBA.ModelManagerEnd
IF NOT EXIST SourcePlugin.HEC-RAS GOTO SourcePlugin.HEC-RASEnd
cd SourcePlugin.HEC-RAS
hg up -r b0fae21b0fbf
cd ..
:SourcePlugin.HEC-RASEnd
IF NOT EXIST SourcePlugin.Lucicat GOTO SourcePlugin.LucicatEnd
cd SourcePlugin.Lucicat
hg up -r e303c20d41c4
cd ..
:SourcePlugin.LucicatEnd
IF NOT EXIST Source.CommunityPlugins GOTO Source.CommunityPluginsEnd
cd Source.CommunityPlugins
hg up -r 79d34eed2f07
cd ..
:Source.CommunityPluginsEnd
IF NOT EXIST SydneyWater.Plugins GOTO SydneyWater.PluginsEnd
cd SydneyWater.Plugins
hg up -r c05b562ae92c
cd ..
:SydneyWater.PluginsEnd
IF NOT EXIST SourcePlugin.AllocationAndUtilities GOTO SourcePlugin.AllocationAndUtilitiesEnd
cd SourcePlugin.AllocationAndUtilities
hg up -r ef7a9e48ed71
cd ..
:SourcePlugin.AllocationAndUtilitiesEnd
IF NOT EXIST MUSICX GOTO MUSICXEnd
cd MUSICX
hg up -r 92d8e2cd7be4
cd ..
:MUSICXEnd
IF NOT EXIST SourcePlugin.J2K GOTO SourcePlugin.J2KEnd
cd SourcePlugin.J2K
hg up -r 0f7652f6c12b
cd ..
:SourcePlugin.J2KEnd
IF NOT EXIST DevelopmentTools GOTO DevelopmentToolsEnd
cd DevelopmentTools
hg up -r 6faa48f1dbb2
cd ..
:DevelopmentToolsEnd
IF NOT EXIST guiAutomation GOTO guiAutomationEnd
cd guiAutomation
hg up -r ec8578ddb66e
cd ..
:guiAutomationEnd
IF NOT EXIST sourceRegressionTests GOTO sourceRegressionTestsEnd
cd sourceRegressionTests
git checkout 912546e2bd2b286513f2f053a30ed210c03e604f
cd ..
:sourceRegressionTestsEnd
IF NOT EXIST sourceRegressionTestsInternal GOTO sourceRegressionTestsInternalEnd
cd sourceRegressionTestsInternal
git checkout 41c4edac17d2de4bad1583051d31c2fde7aad25f
cd ..
:sourceRegressionTestsInternalEnd
IF NOT EXIST musicConversionTests GOTO musicConversionTestsEnd
cd musicConversionTests
git checkout 9c78acc727617e111ee3da6a383d93f85e31bdd5
cd ..
:musicConversionTestsEnd
IF NOT EXIST userInterfaceAutomationProjects GOTO userInterfaceAutomationProjectsEnd
cd userInterfaceAutomationProjects
git checkout 98f791f786420b0286ac17e9595bd5b9a1e7accf
cd ..
:userInterfaceAutomationProjectsEnd
